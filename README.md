# Automation API 
Projeto de teste  - Projeto na branch MASTER

## IDE Utilizada na criação do desenvolvimento:
Eclipse
## Pré-requisitos 
JAVA : Java JDK 1.8

Maven : apache maven 3.8.1(configurado na máquina local)

## Configurações Variáveis de ambiente do usuário
Nome da variável: JAVA_HOME

Java JDK Caminho: C:\Program Files\Java\jdk1.8.0_201 

## Configurações Variáveis de ambiente do sistema(Path)
Editar variável "Path" e adicionar dois novos caminhos : %JAVA_HOME%\bin , 

C:\Users\Public\apache-maven-3.8.1\bin

[Apache Maven 3.8.1 Download] - https://drive.google.com/drive/folders/1BXBe5y6GJVu_ym9S3_AX5qGBP0D-AObg?usp=sharing

## Executar via RunnerTest
Após importar o projeto de tipo "Maven" e buildar com sucesso , 
abrir o RunnerTest localizado em  "src/test/java/steps.runner/Runner" e clicar com o botão  ir em Run as > JUnit Test (OBS: configurar JUNIT para versao 4).

## Executar via Command Line
Abrir um prompt de comando na pasta do projeto ,
Executar o comando : "mvn clean verify" para executar todos os cenários.

Cenários API: @api(tag global), @login, @loginUnsuccessful, @cadastroApi,
@singleUser, @singleUserNotFound, @allUsersList, @singleUserUpdate

## Evidências
Após execução dos testes a evidência em JSON é localizado após finalização da execução dentro da pasta "target/cucumber.json".
